---
sidebar_position: 1
---

# BPMN

BPMN merupakan singkatan dari Business Process Modeling Notation, representasi grafis untuk menentukan proses bisnis dalam model proses bisnis. BPMN bisa dibilang merupakan flowchart dengan standarisasi notasi.

Berikut merupakan BPMN dari modul pendaftaran Hub Arkatama.

![img alt](/img/bpmnver2.png)
