---
sidebar_position: 1
---

# Test Scenario

**Test Scenario** merupakan informasi yang berisi summary scenario dari fitur yang akan dilakukan testing atau pengujian. Pada test scenario ditulis secara umum dan tidak spesifik.

Berikut merupakan test scenario dari Modul Pendaftaran Hub Arkatama.
Baca selengkapnya [Test Scenario Modul Pendaftaran Hub Arkatama](https://docs.google.com/spreadsheets/d/1WNl2S8vpCquZFoEff5WACpUyw1ns3QxQ/edit?usp=sharing&ouid=103390351764628136729&rtpof=true&sd=true)

|        |  **Modul**  |           **Fitur**           | **Test Scenario ID** |                                                  **Test scenario**                                                   |
| :----: | :---------: | :---------------------------: | :------------------: | :------------------------------------------------------------------------------------------------------------------: |
| **1**  | Pendaftaran | Pendaftaran Akun Hub Arkatama |   TC_DaftarAkun_01   |        Mengisi form pendaftaran akun Hub Arkatama dengan inputan data lengkap sesuai dengan yang ada di form         |
| **2**  |             |                               |   TC_DaftarAkun_02   |                 Mengisi form pendaftaran akun Hub Arkatama dengan mengosongkan salah satu mandatory.                 |
| **3**  |             |                               |   TC_DaftarAkun_03   |                    Mengisi form pendaftaran akun Hub Arkatama dengan mengisi berbagai tipe data.                     |
| **4**  |             |                               |   TC_DaftarAkun_04   |             Mengisi form pendaftaran akun Hub Arkatama dengan mengisi lalu melihat dari panjang datanya.             |
| **5**  |             |                               |   TC_DaftarAkun_05   | Mengisi form pendaftaran akun Hub Arkatama dengan konfirmasi password yang tidak sesuai dengan password yang dibuat. |
| **6**  |    Login    |          Log in akun          |     TC_Login_01      |                 Mengisi form login akun Hub Arkatama dengan inputan data sesuai dengan akun pengguna                 |
| **7**  |             |                               |     TC_Login_02      |                    Mengisi form login akun Hub Arkatama dengan mengosongkan salah satu mandatory.                    |
| **8**  |             |                               |     TC_Login_03      |                       Mengisi form login akun Hub Arkatama dengan mengisi berbagai tipe data.                        |
| **9**  |             |                               |     TC_Login_04      |                Mengisi form login akun Hub Arkatama dengan mengisi lalu melihat dari panjang datanya.                |
| **10** |             |                               |     TC_Login_05      |                   Mengisi form login akun Hub Arkatama dengan mengisi tidak sesuai dengan database                   |
| **11** |             |          Ingat saya           |   TC_IngatSaya_01    |                         Mengisi checkbox fitur Ingat Saya untuk menyimpan data dengan mudah                          |
| **12** |             |         Lupa password         |    TC_LupaPass_01    |                               Mengakses fitur 'Lupa Password' untuk mengganti password                               |
| **13** |             |                               |    TC_LupaPass_02    |                        Mengisi form nomor HP dengan tipe data tidak sesuai dengan ketentuan.                         |
| **14** |             |                               |    TC_LupaPass_03    |                    Mengisi form nomor HP dengan panjang data yang tidak sesuai dengan ketentuan.                     |
| **15** |  Data Diri  |     Pilihan Opsi Pelamar      |  TC_OpsiPelamar_01   |                              Memilih salah satu dari opsi pelamar yang telah disediakan                              |
| **16** |             |            Profil             |   TC_DataProfil_01   |                           Melengkapi data diri dan dokumen pada form yang telah disediakan                           |
| **17** |             |                               |   TC_DataProfil_02   |                   Mengisi form profil akun Hub Arkatama dengan mengosongkan salah satu mandatory.                    |
| **18** |             |                               |   TC_DataProfil_03   |                       Mengisi form profil akun Hub Arkatama dengan mengisi berbagai tipe data.                       |
| **19** |             |                               |   TC_DataProfil_04   |               Mengisi form profil akun Hub Arkatama dengan mengisi lalu melihat dari panjang datanya.                |
| **20** |             |                               |   TC_DataProfil_05   |         Mengisi form profil akun Hub Arkatama dengan melihat dari pilihan dropdown yang saling berhubungan.          |
| **21** |             |            Detail             |   TC_DataDetail_01   |                       Melengkapi data diri dan dokumen detail pada form yang telah disediakan                        |
| **22** |             |                               |   TC_DataDetail_02   |                   Mengisi form detail akun Hub Arkatama dengan mengosongkan salah satu mandatory.                    |
| **23** |             |                               |   TC_DataDetail_03   |                       Mengisi form detail akun Hub Arkatama dengan mengisi berbagai tipe data.                       |
| **24** |             |                               |   TC_DataDetail_04   |               Mengisi form detail akun Hub Arkatama dengan mengisi lalu melihat dari panjang datanya.                |
| **25** |             |                               |   TC_DataDetail_05   |         Mengisi form detail akun Hub Arkatama dengan melihat dari pilihan dropdown yang saling berhubungan.          |
| **26** |             |                               |   TC_DataDetail_06   |                         Mengisi form detail akun Hub Arkatama dengan mengosongkan opsional.                          |
