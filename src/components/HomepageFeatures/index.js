import React from "react";
import clsx from "clsx";
import styles from "./styles.module.css";

const FeatureList = [
  {
    title: "About Me",
    Svg: require("@site/static/img/about_me.svg").default,
    description: (
      <>
        I’m a undergraduate from Information System at Universitas Brawijaya. I
        can work well in a team, have good communication, diligent, and care
        about details. With some of the experiences I already have, I am
        optimistic that I can make an impact in my workplace.
      </>
    ),
  },
  {
    title: "My Portfolio",
    Svg: require("@site/static/img/portfolio.svg").default,
    description: (
      <>
        I have a high will to develop my career in the world of information
        technology, especially Software Tester and UI/UX Design. Due to my
        interest in UI/UX design, I have a portfolio of UI designers. You can
        see my portfolio on the link{" "}
        <a href="http://bit.ly/salsabila_portfolio">Salsa's Portofolio</a>
      </>
    ),
  },
  /*{
    title: 'Powered by React',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        Extend or customize your website layout by reusing React. Docusaurus can
        be extended while reusing the same header and footer.
      </>
    ),
  },*/
];

function Feature({ Svg, title, description }) {
  return (
    <div className={clsx("col col--6")}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
